# 知识库

#### 介绍
知识库： 是一个基于开源软件的轻量级的企业知识分享平台，可用于快速构建企业和团队的知识分享平台。部署方便，使用简单，用于构建一个信息共享、文档管理的协作环境。

#### 软件架构
###### 1.支持平台：windows
###### 2.数据库：mysql-5.7.37（需自行下载安装，并建立相应的数据库）
###### 3.开发软件：go 1.8
###### 4.IDE编辑器：goland-2022.3

#### 安装教程

###### 1.打开文件夹：wiki/tree/master/install
###### 2.双击运行：install.exe
###### 3.浏览器输入对应网址，开始安装
###### 4.按步骤说明，填入创建的数据库信息，开始安装

#### 使用说明

1.  安装说明
![输入图片说明](static/images/preview/01.png)
![输入图片说明](static/images/preview/02.png)
![输入图片说明](static/images/preview/03.png)
![输入图片说明](static/images/preview/04.png)

#### 问题联系
![输入图片说明](static/images/preview/wechat_1.jpg)
